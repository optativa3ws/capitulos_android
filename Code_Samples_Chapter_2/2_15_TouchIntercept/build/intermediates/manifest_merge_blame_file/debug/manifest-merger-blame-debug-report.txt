1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.touchintercept"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="9"
8-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml
9        android:targetSdkVersion="24" />
9-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml
10
11    <application
11-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:7:5-20:19
12        android:allowBackup="true"
12-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:8:9-35
13        android:debuggable="true"
14        android:icon="@drawable/ic_launcher"
14-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:9:9-45
15        android:label="@string/app_name"
15-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:10:9-41
16        android:theme="@style/AppTheme" >
16-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:11:9-40
17        <activity
17-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:12:9-19:20
18            android:name="com.androidrecipes.touchintercept.DisallowActivity"
18-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:13:13-45
19            android:label="@string/app_name" >
19-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:14:13-45
20            <intent-filter>
20-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:15:13-18:29
21                <action android:name="android.intent.action.MAIN" />
21-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:16:17-68
21-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:16:25-66
22
23                <category android:name="android.intent.category.LAUNCHER" />
23-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:17:17-76
23-->/home/cbpaguay/android-recipes-5ed/Code_Samples_Chapter_2/2_15_TouchIntercept/src/main/AndroidManifest.xml:17:27-74
24            </intent-filter>
25        </activity>
26    </application>
27
28</manifest>
