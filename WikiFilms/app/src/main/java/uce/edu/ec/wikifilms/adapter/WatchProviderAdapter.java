package uce.edu.ec.wikifilms.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import uce.edu.ec.wikifilms.R;
import uce.edu.ec.wikifilms.model.ModelWatchProvider;
import uce.edu.ec.wikifilms.networking.ApiEndpoint;

public class WatchProviderAdapter extends RecyclerView.Adapter<WatchProviderAdapter.ViewHolder> {

    private List<ModelWatchProvider> items;
    private Context mContext;

    public WatchProviderAdapter(Context context, List<ModelWatchProvider> items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public WatchProviderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_watchprovider, parent, false);
        return new WatchProviderAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WatchProviderAdapter.ViewHolder holder, int position) {
        final ModelWatchProvider data = items.get(position);

        Glide.with(mContext)
                .load(ApiEndpoint.URLIMAGE + data.getLogo_path())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_image)
                        .transform(new RoundedCorners(16)))
                .into(holder.imgWatchProvider);
    }

    @Override
    public int getItemCount() {
        int a ;
        if(items != null && !items.isEmpty()) {

            a = items.size();
        }
        else {
            a = 0;
        }
        return a;
    }

    //Class Holder
    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgWatchProvider;

        public ViewHolder(View itemView) {
            super(itemView);
            imgWatchProvider = itemView.findViewById(R.id.img_watchprovider);
        }
    }

}
