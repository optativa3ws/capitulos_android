package uce.edu.ec.wikifilms.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import uce.edu.ec.wikifilms.R;
import uce.edu.ec.wikifilms.adapter.VideoTrailerAdapter;
import uce.edu.ec.wikifilms.adapter.WatchProviderAdapter;
import uce.edu.ec.wikifilms.database.sqlite.OperacionesBaseDatos;
import uce.edu.ec.wikifilms.model.ModelMovie;
import uce.edu.ec.wikifilms.model.ModelTrailer;
import uce.edu.ec.wikifilms.model.ModelWatchProvider;
import uce.edu.ec.wikifilms.networking.ApiEndpoint;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ivbaranov.mfb.MaterialFavoriteButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class DetailMovieActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvTitle, tvName, tvRating, tvRelease, tvPopularity, tvOverview, tvWatchProviders;
    ImageView imgCover, imgPhoto;
    RecyclerView rvWatchProviders, rvVideoTrailer;
    MaterialFavoriteButton imgFavorite;
    FloatingActionButton fabShare;
    RatingBar ratingBar;
    String NameFilm, ReleaseDate, Popularity, Overview, Cover, Thumbnail, movieURL;
    int Id;
    double Rating;
    ModelMovie modelMovie;
    ProgressDialog progressDialog;
    List<ModelTrailer> modelTrailer = new ArrayList<>();
    List<ModelWatchProvider> modelWatchProviders = new ArrayList<>();
    VideoTrailerAdapter videoTrailerAdapter;
    WatchProviderAdapter watchProviderAdapter;
    private LinearLayout llProgressBar;


    OperacionesBaseDatos dbHelper;

    List<ModelMovie> listaPeliculas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        llProgressBar = findViewById(R.id.llProgressBarDetail);
        llProgressBar.setVisibility(View.VISIBLE);

        /*progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Espere por favor");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Cargando datos");*/

        ratingBar = findViewById(R.id.ratingBar);
        imgCover = findViewById(R.id.imgCover);
        imgPhoto = findViewById(R.id.imgPhoto);
        imgFavorite = findViewById(R.id.imgFavorite);
        tvTitle = findViewById(R.id.tvTitle);
        tvName = findViewById(R.id.tvName);
        tvRating = findViewById(R.id.tvRating);
        tvRelease = findViewById(R.id.tvRelease);
        tvPopularity = findViewById(R.id.tvPopularity);
        tvOverview = findViewById(R.id.tvOverview);
        tvWatchProviders = findViewById(R.id.tv_watchproviders);
        rvVideoTrailer = findViewById(R.id.rvVideoTrailer);
        rvWatchProviders = findViewById(R.id.rvWatchProv);
        fabShare = findViewById(R.id.fabShare);

        //Crear instancia a base de datos
        dbHelper = OperacionesBaseDatos.obtenerInstancia(this);

        //Listar Peliculas Favoritas
        listaPeliculas = dbHelper.listarPeliculas();

        //Obtener pelicula seleccionada del listado de Populares o En Cartelera
        modelMovie = (ModelMovie) getIntent().getSerializableExtra("detailMovie");
        if (modelMovie != null) {
            Id = modelMovie.getId();
            NameFilm = modelMovie.getTitle();
            Rating = modelMovie.getVoteAverage();
            ReleaseDate = modelMovie.getReleaseDate();
            Popularity = modelMovie.getPopularity();
            Overview = modelMovie.getOverview();
            Cover = modelMovie.getBackdropPath();
            Thumbnail = modelMovie.getPosterPath();
            movieURL = ApiEndpoint.URLFILM + "" + Id;

            tvTitle.setText(NameFilm);
            tvName.setText(NameFilm);
            //Formatear número a 1 decimal
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(1);
            tvRating.setText(df.format(Rating) + "/10");
            tvRelease.setText(ReleaseDate);
            tvPopularity.setText(Popularity);
            tvOverview.setText(Overview);
            tvTitle.setSelected(true);
            tvName.setSelected(true);

            float newValue = (float)Rating;
            ratingBar.setNumStars(5);
            ratingBar.setStepSize((float) 0.5);
            ratingBar.setRating(newValue / 2);

            //Renderizar poster de la película
            Glide.with(this)
                    .load(ApiEndpoint.URLIMAGE + Cover)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgCover);

            //Renderizar backdrop de la película
            Glide.with(this)
                    .load(ApiEndpoint.URLIMAGE + Thumbnail)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgPhoto);

            //Modificar lista de proveedores de la película para que se muestren horizontalmente
            rvVideoTrailer.setHasFixedSize(true);
            rvVideoTrailer.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

            //Modificar lista de trailers o videos de la película para que se muestren horizontalmente
            rvWatchProviders.setHasFixedSize(true);
            rvWatchProviders.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

            //Mostrar proveedores y trailers de la película
            getWatchProviders();
            getTrailer();

        }

        //Cambiar estado de FavoriteButton a true si la película está en la base de Datos
        for (ModelMovie peliculas : listaPeliculas) {
            if (peliculas.getId() == Id) {
                imgFavorite.setFavorite(true);
            }
        }

        //Añadir acción al botón de favoritos, para que guarde la película en la base de datos
        imgFavorite.setOnFavoriteChangeListener(
                new MaterialFavoriteButton.OnFavoriteChangeListener() {
                    @Override
                    public void onFavoriteChanged(MaterialFavoriteButton buttonView, boolean favorite) {
                        if (favorite) {
                            Id = modelMovie.getId();
                            NameFilm = modelMovie.getTitle();
                            Rating = modelMovie.getVoteAverage();
                            Overview = modelMovie.getOverview();
                            ReleaseDate = modelMovie.getReleaseDate();
                            Thumbnail = modelMovie.getPosterPath();
                            Cover = modelMovie.getBackdropPath();
                            Popularity = modelMovie.getPopularity();
                            dbHelper.insertarPelicula(new ModelMovie(Id, NameFilm, Rating, Overview, ReleaseDate, Thumbnail, Cover, Popularity));
                            Snackbar.make(buttonView, modelMovie.getTitle() + " Añadido a Favoritos",
                                    Snackbar.LENGTH_SHORT).show();
                        } else {
                            dbHelper.eliminarPelicula(modelMovie.getId());
                            Snackbar.make(buttonView, modelMovie.getTitle() + " Eliminado de Favoritos",
                                    Snackbar.LENGTH_SHORT).show();
                        }

                    }
                }
        );

        //Añadir acción al botón flotante de compartir
        fabShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                String subject = modelMovie.getTitle();
                String description = modelMovie.getOverview();
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                shareIntent.putExtra(Intent.EXTRA_TEXT, subject + "\n\n" + description + "\n\n" + movieURL);
                startActivity(Intent.createChooser(shareIntent, "Compartir con :"));
            }
        });

    }

    //Método para obtener los trailers de la película desde la API
    private void getTrailer() {
        //progressDialog.show();
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.MOVIE_VIDEO + ApiEndpoint.APIKEY + ApiEndpoint.LANGUAGE)
                .addPathParameter("id", String.valueOf(Id))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ModelTrailer dataApi = new ModelTrailer();
                                dataApi.setKey(jsonObject.getString("key"));
                                dataApi.setType(jsonObject.getString("type"));
                                dataApi.setName(jsonObject.getString("name"));
                                modelTrailer.add(dataApi);
                            }
                            //progressDialog.dismiss();
                            showVideoTrailer();
                            llProgressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            llProgressBar.setVisibility(View.GONE);
                            Toast.makeText(DetailMovieActivity.this,
                                    "No se pudieron mostrar los datos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        llProgressBar.setVisibility(View.GONE);
                        Toast.makeText(DetailMovieActivity.this,
                                "No hay conexión a internet!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //Método para obtener las plataformas donde se puede ver o comprar la película desde la API
    private void getWatchProviders() {
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.MOVIE_PROVIDERS + ApiEndpoint.APIKEY)
                .addPathParameter("id", String.valueOf(Id))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.getString("results"));
                            if(jsonObject.has("EC")) {
                                JSONObject jsonObjectEC = new JSONObject(jsonObject.getString("EC"));
                                if(jsonObjectEC.has("flatrate")) {
                                    JSONArray jsonArray = jsonObjectEC.getJSONArray("flatrate");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                                        ModelWatchProvider dataApi = new ModelWatchProvider();
                                        System.out.println(jsonObj.getString("logo_path"));
                                        dataApi.setProvider_name(jsonObj.getString("provider_name"));
                                        dataApi.setLogo_path(jsonObj.getString("logo_path"));
                                        modelWatchProviders.add(dataApi);
                                    }
                                    showWatchProviders();
                                }else if(jsonObjectEC.has("buy")){
                                    JSONArray jsonArray = jsonObjectEC.getJSONArray("buy");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                                        ModelWatchProvider dataApi = new ModelWatchProvider();
                                        System.out.println(jsonObj.getString("logo_path"));
                                        dataApi.setProvider_name(jsonObj.getString("provider_name"));
                                        dataApi.setLogo_path(jsonObj.getString("logo_path"));
                                        modelWatchProviders.add(dataApi);
                                    }
                                    tvWatchProviders.setText(R.string.disponible_compra);
                                    showWatchProviders();
                                }else if(jsonObjectEC.has("rent")){
                                    JSONArray jsonArray = jsonObjectEC.getJSONArray("rent");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                                        ModelWatchProvider dataApi = new ModelWatchProvider();
                                        System.out.println(jsonObj.getString("logo_path"));
                                        dataApi.setProvider_name(jsonObj.getString("provider_name"));
                                        dataApi.setLogo_path(jsonObj.getString("logo_path"));
                                        modelWatchProviders.add(dataApi);
                                    }
                                    tvWatchProviders.setText(R.string.disponible_renta);
                                    showWatchProviders();
                                }
                            }else {
                                tvWatchProviders.setTypeface(null, Typeface.ITALIC);
                                tvWatchProviders.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                tvWatchProviders.setText(R.string.no_disponible);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            llProgressBar.setVisibility(View.GONE);
                            System.out.println("ERROR JSON: "+e.toString());
                            //Toast.makeText(DetailMovieActivity.this,
                            // "No se pudieron mostrar los datos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        llProgressBar.setVisibility(View.GONE);
                        System.out.println("ON ERROR: "+anError.getErrorBody());
                        //Toast.makeText(DetailMovieActivity.this,
                         //       "No hay conexión a internet!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //Método para llamar al Adapter que se encarga de renderizar las imagenes de los trailers de la pelicula
    private void showVideoTrailer(){
        videoTrailerAdapter = new VideoTrailerAdapter(DetailMovieActivity.this, modelTrailer);
        rvVideoTrailer.setAdapter(videoTrailerAdapter);
    }

    //Método para llamar al Adapter que se encarga de renderizar las imagenes de las plataformas donde se puede ver
    // o comprar la pelicula
    private void showWatchProviders() {
        watchProviderAdapter = new WatchProviderAdapter(DetailMovieActivity.this, modelWatchProviders);
        rvWatchProviders.setAdapter(watchProviderAdapter);
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams winParams = window.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        window.setAttributes(winParams);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
