package uce.edu.ec.wikifilms.database.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.provider.BaseColumns;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "filmsdata.db";
    private static final int DATABASE_VERSION = 1;

    private final Context contexto;

    interface Tablas {
        String FILM = "film";
        String TIPO = "tipo";
    }

    public DataBaseHelper(Context contexto) {
        super(contexto, DATABASE_NAME, null, DATABASE_VERSION);
        this.contexto = contexto;
    }

    interface Referencias {
        String ID_TIPO = String.format("REFERENCES %s(%s)",
                Tablas.TIPO, ContratoMedia.Tipos.ID);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (%s TEXT UNIQUE NOT NULL," +
                        "%s TEXT)",
                Tablas.TIPO, ContratoMedia.Tipos.ID,
                ContratoMedia.Tipos.NOMBRE));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,"+
                        "%s INTEGER NOT NULL," +
                        "%s TEXT, %s REAL, %s TEXT, %s TEXT, " +
                        "%s TEXT, %s TEXT, %s TEXT, "+
                        "%s TEXT NOT NULL %s)",
                Tablas.FILM, BaseColumns._ID,
                ContratoMedia.Films.ID,
                ContratoMedia.Films.TITULO, ContratoMedia.Films.VOTOS_AVERAGE,
                ContratoMedia.Films.RESUMEN, ContratoMedia.Films.FECHA,
                ContratoMedia.Films.POSTER_PATH, ContratoMedia.Films.BACKDROP_PATH,
                ContratoMedia.Films.POPULARIDAD,
                ContratoMedia.Films.ID_TIPO, Referencias.ID_TIPO));

        //Agregar Tipos por defecto
        db.execSQL(String.format("INSERT INTO %s VALUES ('1', 'Películas')",
                Tablas.TIPO));
        db.execSQL(String.format("INSERT INTO %s VALUES ('2', 'Series')",
                Tablas.TIPO));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Tablas.FILM);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.TIPO);

        onCreate(db);
    }
}
