package uce.edu.ec.wikifilms.database.sqlite;

/**
 * Clase que establece los nombres a usar en la base de datos
 */
public class ContratoMedia {

    interface ColumnasFilm{
        String ID = "id";
        String TITULO = "titulo";
        String VOTOS_AVERAGE = "avg_votos";
        String RESUMEN = "resumen";
        String FECHA = "fecha";
        String POSTER_PATH = "poster_path";
        String BACKDROP_PATH = "bd_path";
        String POPULARIDAD = "popularidad";
        String ID_TIPO = "id_tipo";
    }

    interface ColumnasTipo{
        String ID = "id";
        String NOMBRE = "nombre";
    }

    public static class Films implements  ColumnasFilm{
    }

    public static class Tipos implements ColumnasTipo{
    }

    private ContratoMedia() {
    }

}
