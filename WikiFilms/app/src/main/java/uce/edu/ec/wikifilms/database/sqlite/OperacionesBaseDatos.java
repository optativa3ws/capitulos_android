package uce.edu.ec.wikifilms.database.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import uce.edu.ec.wikifilms.model.ModelMovie;
import uce.edu.ec.wikifilms.database.sqlite.ContratoMedia.Films;
import uce.edu.ec.wikifilms.database.sqlite.ContratoMedia.Tipos;
import uce.edu.ec.wikifilms.database.sqlite.DataBaseHelper.Tablas;
import uce.edu.ec.wikifilms.model.ModelTV;

import java.util.ArrayList;
import java.util.List;

public final class OperacionesBaseDatos {

    private static DataBaseHelper dbHelper;

    private static final OperacionesBaseDatos instancia = new OperacionesBaseDatos();

    public OperacionesBaseDatos() {
    }

    public static OperacionesBaseDatos obtenerInstancia(Context contexto) {
        if (dbHelper == null) {
            dbHelper = new DataBaseHelper(contexto);
        }
        return instancia;
    }

    //PELICULAS
    public int insertarPelicula(ModelMovie pelicula) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Films.ID, pelicula.getId());
        valores.put(Films.TITULO, pelicula.getTitle());
        valores.put(Films.VOTOS_AVERAGE, pelicula.getVoteAverage());
        valores.put(Films.RESUMEN, pelicula.getOverview());
        valores.put(Films.FECHA, pelicula.getReleaseDate());
        valores.put(Films.POSTER_PATH, pelicula.getPosterPath());
        valores.put(Films.BACKDROP_PATH, pelicula.getBackdropPath());
        valores.put(Films.POPULARIDAD, pelicula.getPopularity());
        valores.put(Films.ID_TIPO, "1");

        // Insertar pelicula
        db.insertOrThrow(Tablas.FILM, null, valores);

        return pelicula.getId();
    }

    public void eliminarPelicula(int idPelicula) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String whereClause = Films.ID + "=?" + " AND " + Films.ID_TIPO + "=?";
        String[] whereArgs = {String.valueOf(idPelicula), "1"};

        db.delete(Tablas.FILM, whereClause, whereArgs);
    }

    public List<ModelMovie> listarPeliculas() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE ID_TIPO ='1'", Tablas.FILM);

        Cursor cursor = db.rawQuery(sql, null);
        List<ModelMovie> lista = new ArrayList<>();
        int id;
        String titulo;
        Float avg_votos;
        String resumen;
        String fecha;
        String poster_path;
        String bd_path;
        String popularidad;
        ModelMovie pelicula;
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            id = cursor.getInt(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            avg_votos = cursor.getFloat(cursor.getColumnIndex("avg_votos"));
            resumen = cursor.getString(cursor.getColumnIndex("resumen"));
            fecha = cursor.getString(cursor.getColumnIndex("fecha"));
            poster_path = cursor.getString(cursor.getColumnIndex("poster_path"));
            bd_path = cursor.getString(cursor.getColumnIndex("bd_path"));;
            popularidad = cursor.getString(cursor.getColumnIndex("popularidad"));;
            pelicula = new ModelMovie(id, titulo, avg_votos, resumen, fecha, poster_path, bd_path, popularidad);
            lista.add(pelicula);
            cursor.moveToNext();
        }

        return lista;
    }


    //SERIES
    public int insertarSerie(ModelTV serie) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put(Films.ID, serie.getId());
        valores.put(Films.TITULO, serie.getName());
        valores.put(Films.VOTOS_AVERAGE, serie.getVoteAverage());
        valores.put(Films.RESUMEN, serie.getOverview());
        valores.put(Films.FECHA, serie.getReleaseDate());
        valores.put(Films.POSTER_PATH, serie.getPosterPath());
        valores.put(Films.BACKDROP_PATH, serie.getBackdropPath());
        valores.put(Films.POPULARIDAD, serie.getPopularity());
        valores.put(Films.ID_TIPO, "2");

        // Insertar pelicula
        db.insertOrThrow(Tablas.FILM, null, valores);

        return serie.getId();
    }

    public void eliminarSerie(int idSerie) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String whereClause = Films.ID + "=?" + " AND " + Films.ID_TIPO + "=?";
        String[] whereArgs = {String.valueOf(idSerie), "2"};

        db.delete(Tablas.FILM, whereClause, whereArgs);
    }

    public List<ModelTV> listarSeries() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String sql = String.format("SELECT * FROM %s WHERE ID_TIPO ='2'", Tablas.FILM);

        Cursor cursor = db.rawQuery(sql, null);
        List<ModelTV> lista = new ArrayList<>();
        int id;
        String titulo;
        Float avg_votos;
        String resumen;
        String fecha;
        String poster_path;
        String bd_path;
        String popularidad;
        ModelTV pelicula;
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            id = cursor.getInt(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
            avg_votos = cursor.getFloat(cursor.getColumnIndex("avg_votos"));
            resumen = cursor.getString(cursor.getColumnIndex("resumen"));
            fecha = cursor.getString(cursor.getColumnIndex("fecha"));
            poster_path = cursor.getString(cursor.getColumnIndex("poster_path"));
            bd_path = cursor.getString(cursor.getColumnIndex("bd_path"));;
            popularidad = cursor.getString(cursor.getColumnIndex("popularidad"));;
            pelicula = new ModelTV(id, titulo, avg_votos, resumen, fecha, poster_path, bd_path, popularidad);
            lista.add(pelicula);
            cursor.moveToNext();
        }

        return lista;
    }
}