package uce.edu.ec.wikifilms.model;

public class ModelWatchProvider {

    private String logo_path;
    private String provider_name;

    public String getLogo_path() {
        return logo_path;
    }

    public void setLogo_path(String logo_path) {
        this.logo_path = logo_path;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public void setProvider_name(String provider_name) {
        this.provider_name = provider_name;
    }

    @Override
    public String toString() {
        return "ModelWatchProvider{" +
                "logo_path='" + logo_path + '\'' +
                ", provider_name='" + provider_name + '\'' +
                '}';
    }
}
