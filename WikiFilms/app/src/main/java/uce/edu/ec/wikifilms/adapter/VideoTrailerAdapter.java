package uce.edu.ec.wikifilms.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import uce.edu.ec.wikifilms.R;
import uce.edu.ec.wikifilms.model.ModelTrailer;
import uce.edu.ec.wikifilms.networking.ApiEndpoint;

public class VideoTrailerAdapter extends RecyclerView.Adapter<VideoTrailerAdapter.ViewHolder> {

    private List<ModelTrailer> items;
    private Context mContext;

    public VideoTrailerAdapter(Context context, List<ModelTrailer> items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public VideoTrailerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_video, parent, false);
        return new VideoTrailerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoTrailerAdapter.ViewHolder holder, int position) {
        final ModelTrailer data = items.get(position);

        Glide.with(mContext)
                .load(ApiEndpoint.YT_VIDEO + data.getKey() + "/0.jpg")
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .apply(new RequestOptions()
                        .placeholder(R.drawable.ic_image)
                        .transform(new RoundedCorners(16)))
                .into(holder.imgTrailer);
        holder.imgPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.youtube.com/watch?v=" + data.getKey()));
                mContext.startActivity(intent);
            }
        });
        holder.txtTrailerTitle.setText(data.getName());
    }

    @Override
    public int getItemCount() {
        int a ;
        if(items != null && !items.isEmpty()) {

            a = items.size();
        }
        else {
            a = 0;
        }
        return a;
    }

    //Class Holder
    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgTrailer;

        public ImageButton imgPlayButton;

        public TextView txtTrailerTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imgTrailer = itemView.findViewById(R.id.youTubeThumbnailView);
            imgPlayButton = itemView.findViewById(R.id.playButton);
            txtTrailerTitle = itemView.findViewById(R.id.trailerTitleTxtView);
        }
    }

}
