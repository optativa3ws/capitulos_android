package uce.edu.ec.wikifilms.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

import uce.edu.ec.wikifilms.R;

public class FragmentMap extends Fragment {

    SupportMapFragment supportMapFragment;

    GoogleMap map;

    FusedLocationProviderClient fusedLocationProviderClient;

    LinearLayout llProgressBar;

    double currentLat = 0, currentLong = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);


        supportMapFragment = (SupportMapFragment) this.getChildFragmentManager().
                findFragmentById(R.id.google_map);

        FloatingActionButton fabLoc = rootView.findViewById(R.id.fabLoc);
        llProgressBar = rootView.findViewById(R.id.llProgressBar);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    44);

        }
        getCurrentLocation();

        fabLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?" + //Url
                        "location=" + currentLat + "," + currentLong + //Localizacion, latitud y longitud
                        //"&radius=10000" + //radio
                        "&types=" + "movie_theater" + //Tipos de Lugares
                        "&rankby=" + "prominence" +
                        "&language=" + "es-419" + //Español Latinoamerica
                        "&key=" + getResources().getString(R.string.google_map_apikey);*/
                llProgressBar.setVisibility(View.VISIBLE);

                String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?" + //URL
                        "query=" + "cines" +
                        "&language=" + "es-419" + //Español Latinoamerica
                        "&location=" + currentLat + "," + currentLong + //Localizacion, latitud y longitud
                        "&radius=1000" + //radio
                        "&key=" + getResources().getString(R.string.google_maps_apikey);

                new PlaceTask().execute(url);
            }
        });

        return rootView;
    }

    //Método para obtener la ubicación actual del dispositivo
    public void getCurrentLocation() {
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLat = location.getLatitude();
                    currentLong = location.getLongitude();
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            map = googleMap;
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(currentLat, currentLong), 15));
                            LatLng latLng = new LatLng(currentLat, currentLong);

                            map.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .title("Tu Ubicación Actual")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                        }
                    });
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            } else {
                // Permisos Denegados
                Toast.makeText(getActivity(),"Por favor, Otorgue los Permisos de Ubicación!", Toast.LENGTH_LONG)
                        .show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private class PlaceTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... strings) {
            String data = null;
            try {
                data = downloadUrl(strings[0]);
            } catch (IOException e) {
                System.out.println("Error PlaceTask doInbackground: " + e.toString());
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            new ParserTask().execute(s);
        }
    }

    private String downloadUrl(String string) throws IOException {
        URL url = new URL(string);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.connect();

        InputStream stream = connection.getInputStream();

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        StringBuilder builder = new StringBuilder();

        String line = "";

        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        String data = builder.toString();

        reader.close();

        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        @Override
        protected List<HashMap<String, String>> doInBackground(String... strings) {
            JsonParser jsonParser = new JsonParser();

            List<HashMap<String, String>> mapList = null;
            JSONObject object;
            try {
                object = new JSONObject(strings[0]);

                mapList = jsonParser.parseResult(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return mapList;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> hashMaps) {
            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_cinema);

            for (int i = 0; i < hashMaps.size(); i++) {
                HashMap<String, String> hashMap = hashMaps.get(i);

                double lat = Double.parseDouble(hashMap.get("lat"));
                double lng = Double.parseDouble(hashMap.get("lng"));

                String name = hashMap.get("name");

                String formatted_address = hashMap.get("formatted_address");

                LatLng latLng = new LatLng(lat, lng);

                Marker marker;
                marker = map.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(name)
                            .snippet(formatted_address)
                            .icon(icon));
                //Cines que no queremos que salgan en la aplicación //A ESTOS NO LES CONSIDERAMOS CINES
                if (marker.getTitle().equals("radiacon")
                        || marker.getTitle().equals("Cinemark Ecuador Oficina Corporativa")
                        || marker.getTitle().equals("Virmedia Films")
                        || marker.getTitle().equals("Sala de Cine \"Alfredo Pareja Diezcanseco\"")
                        || marker.getTitle().equals("Flacsocine")
                        || marker.getTitle().equals("Cines El Recreo")
                        || marker.getTitle().equals("Cinemaxec Cortos")) {
                    marker.remove();
                }
            }

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(currentLat, currentLong), 13));

            map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker arg0) {
                    try {
                        if(!arg0.getTitle().equals("Tu Ubicación Actual")) {
                            Uri gmmIntentUri = Uri.parse("geo:" + arg0.getPosition().latitude + "," + arg0.getPosition().longitude + "?q="
                                    + Uri.encode(arg0.getTitle() + " " + arg0.getSnippet()));
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        }
                    }catch(Exception e){
                        Toast.makeText(getActivity(),"No se pudo abrir Google Maps" +
                                "Intente de nuevo, más tarde!", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });

            llProgressBar.setVisibility(View.INVISIBLE);

        }
    }
}
