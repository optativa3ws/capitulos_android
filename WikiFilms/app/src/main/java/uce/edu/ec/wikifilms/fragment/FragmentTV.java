package uce.edu.ec.wikifilms.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import uce.edu.ec.wikifilms.R;
import uce.edu.ec.wikifilms.activities.DetailTelevisionActivity;
import uce.edu.ec.wikifilms.adapter.TvAdapter;
import uce.edu.ec.wikifilms.adapter.TvHorizontalAdapter;
import uce.edu.ec.wikifilms.model.ModelTV;
import uce.edu.ec.wikifilms.networking.ApiEndpoint;

import com.ramotion.cardslider.CardSliderLayoutManager;
import com.ramotion.cardslider.CardSnapHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class FragmentTV extends Fragment implements TvHorizontalAdapter.onSelectData, TvAdapter.onSelectData {

    private RecyclerView rvNowPlaying, rvFilmRecommend;
    private TvHorizontalAdapter tvHorizontalAdapter;
    private TvAdapter tvAdapter;
    private ProgressDialog progressDialog;
    private SearchView searchFilm;
    private TextView textRecomended;
    private TextView textTitle, txtSearch;
    private List<ModelTV> tvPlayNow = new ArrayList<>();
    private List<ModelTV> tvPopular = new ArrayList<>();
    private View rootView;

    private SwipeRefreshLayout movieSRL;
    private LinearLayout llProgressBar;

    public FragmentTV() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_film, container, false);

        /*progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Espere por favor");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Cargando datos");*/

        llProgressBar = rootView.findViewById(R.id.llProgressBarMain);

        txtSearch = rootView.findViewById(R.id.tv_Recommend);

        searchFilm = rootView.findViewById(R.id.searchFilm);
        searchFilm.setQueryHint(getString(R.string.search_tvshow));
        searchFilm.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                txtSearch.setText("Haz buscado: "+query);
                setSearchTv(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.equals(""))
                    getFilmTV();
                txtSearch.setText("Películas Recomendadas");
                return false;
            }
        });

        textRecomended = rootView.findViewById(R.id.tv_Recommend);
        textRecomended.setText(R.string.recomended_tvshow);

        //Cambiar el título de la Vista de Series
        textTitle = rootView.findViewById(R.id.tv_titulo);
        textTitle.setText(R.string.now_tvshow);

        int searchCloseButtonId = searchFilm.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = (ImageView) this.searchFilm.findViewById(searchCloseButtonId);
        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Manage this event.
                rootView.requestFocus();
                hideKeyboard(getActivity());
                searchFilm.setQuery("",false);
            }
        });

        int searchPlateId = searchFilm.getContext().getResources()
                .getIdentifier("android:id/search_plate", null, null);
        View searchPlate = searchFilm.findViewById(searchPlateId);
        if (searchPlate != null) {
            searchPlate.setBackgroundColor(Color.TRANSPARENT);
        }

        rvNowPlaying = rootView.findViewById(R.id.rvNowPlaying);
        rvNowPlaying.setHasFixedSize(true);
        rvNowPlaying.setLayoutManager(new CardSliderLayoutManager(getActivity()));
        new CardSnapHelper().attachToRecyclerView(rvNowPlaying);

        rvFilmRecommend = rootView.findViewById(R.id.rvFilmRecommend);
        rvFilmRecommend.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFilmRecommend.setHasFixedSize(true);

        movieSRL = rootView.findViewById(R.id.srl_movie);
        movieSRL.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                tvPlayNow.clear();
                tvPopular.clear();
                txtSearch.setText("Películas Recomendadas");
                /*progressDialog = new ProgressDialog(getActivity());
                progressDialog.setTitle("Espere por favor");
                progressDialog.setCancelable(false);*/
                rvNowPlaying.setHasFixedSize(true);
                rvNowPlaying.setLayoutManager(new CardSliderLayoutManager(getActivity()));
                getTvHorizontal();
                getFilmTV();
                movieSRL.setRefreshing(false);
            }
        });

        llProgressBar.setVisibility(View.VISIBLE);

        getTvHorizontal();
        getFilmTV();

        return rootView;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void onResume() {
        super.onResume();
        rootView.requestFocus();
    }

    //Método que devolverá el resultado (serie) que el usuario buscará
    private void setSearchTv(String query) {
        //progressDialog.show();
        llProgressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.SEARCH_TV
                + ApiEndpoint.APIKEY + ApiEndpoint.LANGUAGE + ApiEndpoint.QUERY + query)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //progressDialog.dismiss();
                            tvPopular = new ArrayList<>();
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ModelTV dataApi = new ModelTV();
                                SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMMM yyyy", new Locale("es", "EC"));
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd", new Locale("es", "EC"));
                                String datePost;
                                if(jsonObject.has("first_air_date")){
                                    datePost = jsonObject.getString("first_air_date");
                                }else {
                                    datePost = "";
                                }

                                dataApi.setId(jsonObject.getInt("id"));
                                dataApi.setName(jsonObject.getString("name"));
                                dataApi.setVoteAverage(jsonObject.getDouble("vote_average"));
                                dataApi.setOverview(jsonObject.getString("overview"));
                                if(datePost.isEmpty()){
                                    dataApi.setReleaseDate(null);
                                }else{
                                    dataApi.setReleaseDate(formatter.format(dateFormat.parse(datePost)));
                                }
                                dataApi.setPosterPath(jsonObject.getString("poster_path"));
                                dataApi.setBackdropPath(jsonObject.getString("backdrop_path"));
                                dataApi.setPopularity(jsonObject.getString("popularity"));
                                //Las series que contengan poster se añaden a la lista
                                if(!jsonObject.getString("poster_path").equals("null")){
                                    tvPopular.add(dataApi);
                                }
                            }
                            showFilmTV();
                            llProgressBar.setVisibility(View.GONE);
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "No se pudieron mostrar los datos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No hay conexión a internet!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //Método para obtener las series que están "Al Aire" desde la API
    private void getTvHorizontal() {
        //progressDialog.show();
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.TV_PLAYNOW + ApiEndpoint.APIKEY + ApiEndpoint.LANGUAGE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //progressDialog.dismiss();
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ModelTV dataApi = new ModelTV();
                                SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMMM yyyy", new Locale("es", "EC"));
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd", new Locale("es", "EC"));
                                String datePost = jsonObject.getString("first_air_date");

                                dataApi.setId(jsonObject.getInt("id"));
                                dataApi.setName(jsonObject.getString("name"));
                                dataApi.setVoteAverage(jsonObject.getDouble("vote_average"));
                                dataApi.setOverview(jsonObject.getString("overview"));
                                dataApi.setReleaseDate(formatter.format(Objects.requireNonNull(dateFormat.parse(datePost))));
                                dataApi.setPosterPath(jsonObject.getString("poster_path"));
                                dataApi.setBackdropPath(jsonObject.getString("backdrop_path"));
                                dataApi.setPopularity(jsonObject.getString("popularity"));
                                tvPlayNow.add(dataApi);
                            }
                            showMovieHorizontal();
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                            //Toast.makeText(getActivity(), "No se pudieron mostar los datos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No hay conexión a internet!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //Método para obtener las series Recomendadas desde la API
    private void getFilmTV() {
        //progressDialog.show();
        AndroidNetworking.get(ApiEndpoint.BASEURL + ApiEndpoint.TV_POPULAR + ApiEndpoint.APIKEY + ApiEndpoint.LANGUAGE)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //progressDialog.dismiss();
                            tvPopular = new ArrayList<>();
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ModelTV dataApi = new ModelTV();
                                SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMMM yyyy", new Locale("es", "EC"));
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "EC"));
                                String datePost = jsonObject.getString("first_air_date");

                                dataApi.setId(jsonObject.getInt("id"));
                                dataApi.setName(jsonObject.getString("name"));
                                dataApi.setVoteAverage(jsonObject.getDouble("vote_average"));
                                dataApi.setOverview(jsonObject.getString("overview"));
                                dataApi.setReleaseDate(formatter.format(Objects.requireNonNull(dateFormat.parse(datePost))));
                                dataApi.setPosterPath(jsonObject.getString("poster_path"));
                                dataApi.setBackdropPath(jsonObject.getString("backdrop_path"));
                                dataApi.setPopularity(jsonObject.getString("popularity"));
                                tvPopular.add(dataApi);
                            }
                            showFilmTV();
                            llProgressBar.setVisibility(View.GONE);
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "No se pudieron mostar los datos!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //progressDialog.dismiss();
                        Toast.makeText(getActivity(), "No hay conexión a internet!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showMovieHorizontal() {
        tvHorizontalAdapter = new TvHorizontalAdapter(getActivity(), tvPlayNow, this);
        rvNowPlaying.setAdapter(tvHorizontalAdapter);
        tvHorizontalAdapter.notifyDataSetChanged();
    }

    private void showFilmTV() {
        tvAdapter = new TvAdapter(getActivity(), tvPopular, this);
        rvFilmRecommend.setAdapter(tvAdapter);
        tvAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSelected(ModelTV modelTV) {
        Intent intent = new Intent(getActivity(), DetailTelevisionActivity.class);
        intent.putExtra("detailTV", modelTV);
        startActivity(intent);
    }
}
