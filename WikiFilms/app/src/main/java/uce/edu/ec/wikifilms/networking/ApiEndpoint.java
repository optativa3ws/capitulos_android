package uce.edu.ec.wikifilms.networking;

import org.jetbrains.annotations.NotNull;

/**
 * Especificar los parametros para poder consumir la API
 */

public class ApiEndpoint {

    public static String BASEURL = "http://api.themoviedb.org/3/";
    //Introducir la llave de la API TMDB
    public static String APIKEY = "api_key=93240a98e4e2033564a34fe2392139d9";
    public static String LANGUAGE = "&language=es-MX";
    public static String SEARCH_MOVIE = "search/movie?";
    public static String SEARCH_TV = "search/tv?";
    public static String QUERY = "&query=";
    public static String MOVIE_PLAYNOW = "movie/now_playing?";
    public static String MOVIE_POPULAR = "discover/movie?";
    public static String TV_PLAYNOW = "tv/on_the_air?";
    public static String TV_POPULAR = "discover/tv?";
    public static String URLIMAGE = "https://image.tmdb.org/t/p/w780/";
    public static String URLFILM = "https://www.themoviedb.org/movie/";
    public static String NOTIF_DATE = "&primary_release_date.gte=";
    public static String REALESE_DATE = "&primary_release_date.lte=";
    public static String MOVIE_VIDEO = "movie/{id}/videos?";
    public static String TV_VIDEO = "tv/{id}/videos?";
    public static String MOVIE_PROVIDERS = "movie/{id}/watch/providers?";
    public static String TV_PROVIDERS = "tv/{id}/watch/providers?";
    public static String YT_VIDEO = "https://img.youtube.com/vi/";

}
