package uce.edu.ec.fing.crud_pedidos.ui.cabeceraPedido;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;
import uce.edu.ec.fing.crud_pedidos.modelo.FormaPago;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class CabeceraPedidoAddEditActivity extends AppCompatActivity {

    private OperacionesBaseDatos datos;

    private String id_cabecera = null;

    private Spinner spinnerFormaPago, spinnerCliente;

    private EditText txtDate;

    private String fecha;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_cabecera_pedido);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Se obtiene el ID de la Cabecera seleccionada de la lista para poder Actualizar/Eliminar
        id_cabecera = getIntent().getStringExtra("id_cabecera");

        //Se cambia el titulo de la vista en caso de que se quiera Añadir/Actualizar
        setTitle(id_cabecera == null ? "Añadir Cabecera Pedido" : "Editar Cabecera Pedido");

        //Se crea una instancia de la clase que contiene los metodos para persistir los datos
        datos = OperacionesBaseDatos.obtenerInstancia(getApplicationContext());

        //Se obtiene los objetos de la vista que se van a utilizar
        Button btnDatePicker = findViewById(R.id.btn_date);
        Button btnAddEditCabecera = findViewById(R.id.bt_addeditCabecera);

        txtDate = findViewById(R.id.in_date);
        spinnerCliente = findViewById(R.id.clientspinner);
        spinnerFormaPago = findViewById(R.id.formaPagospinner);

        //Se crea el metodo para que cuando el Usuario presione el boton se abra el cuadro de dialogo de la Fecha
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDatePicker();
            }
        });

        //Se cargan los metodos con los datos a los Spinner
        loadClientSpinnerData();
        loadFormaPagoSpinnerData();

        //Si el ID de la cabecera no es nulo, se va a Actualizar o Eliminar
        if (id_cabecera != null) {
            CabeceraPedido cabecera = datos.obtenerCabeceraID(id_cabecera);
            try {
                txtDate.setText(timeEditText(cabecera.getFecha()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Hacemos que sea visible el boton de eliminar
            FloatingActionButton fab_deleteCabecera = findViewById(R.id.fab_deleteCabecera);
            fab_deleteCabecera.setVisibility(View.VISIBLE);

            //Cambiamos el label del botón para que el usuario se guie y pueda presionar si desea Actualizar
            btnAddEditCabecera.setText("ACTUALIZAR");
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //Método para cambiar la fecha al formate que va a persistir en la DB
    public String get_Time(String fecha) {
        int anio, mes, dia;
        Calendar date;
        String[] dateParts = fecha.split("-");
        dia = Integer.parseInt(dateParts[0]);
        mes = Integer.parseInt(dateParts[1]) - 1;
        anio = Integer.parseInt(dateParts[2]);
        date = new GregorianCalendar(anio, mes, dia);
        String CURRENT_TIME_FORMAT = "yyyy MMMM dd";
        Locale local = new Locale("es", "EC");
        SimpleDateFormat dateFormat = new SimpleDateFormat(CURRENT_TIME_FORMAT, local);
        return dateFormat.format(date.getTime());
    }

    //Método para cambiar el formato de fecha que se muestra en el TextEdit de la Vista CabeceraPedidoAddEditActivity
    public String timeEditText(String s) throws ParseException {
        Locale local = new Locale("es", "EC");
        DateFormat dfParse = new SimpleDateFormat("yyyy MMMM dd", local);
        DateFormat dfFormat = new SimpleDateFormat("dd-MM-yyyy",local);
        return dfFormat.format(dfParse.parse(s));
    }

    //Método para mostrar el Calendario al presionar el botón para seleccionar la fecha
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Configurar la fecha actual en el diálogo
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        /**
         * Fijar la devolución de la llamada para capturar la fecha seleccionada
         */
        date.setCallBack(ondate);
        date.show(getSupportFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            fecha = dayOfMonth + "-" + String.format("%02d", monthOfYear + 1)
                    + "-" + year;
            txtDate.setText(fecha);
        }
    };


    /**
     * Método para cargar los CLientes activos en el Spinner correspondiente
     */
    private void loadClientSpinnerData() {
        List<String> labels = datos.getAllClientsActivos();

        // Creación del apadtador para el Spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, labels);

        // Estilo de diseño desplegable - vista de lista con botón de radio
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // adjuntando el adaptador de datos al spinner
        spinnerCliente.setAdapter(dataAdapter);
    }

    /**
     * Método para cargar las Formas de Pago activas en el Spinner correspondiente
     */
    private void loadFormaPagoSpinnerData() {
        List<String> labels = datos.getAllFormaPagoActivas();

        // Creación del apadtador para el Spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, labels);

        // Estilo de diseño desplegable - vista de lista con botón de radio
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // adjuntando el adaptador de datos al spinner
        spinnerFormaPago.setAdapter(dataAdapter);
    }

    public void onClick(View view) {
        fecha = get_Time(txtDate.getText().toString());
        Cliente cliente = datos.getClienteporNombre(spinnerCliente.getSelectedItem().toString());
        FormaPago formaPago = datos.getFormaPagoporNombre(spinnerFormaPago.getSelectedItem().toString());
        System.out.println(id_cabecera);
        if (view.getId() == R.id.bt_addeditCabecera) {
            if (id_cabecera != null) {
                //Se obtiene la cabecera que se quiere Actualizar
                CabeceraPedido cp = datos.getCabeceraPedido(id_cabecera);
                cp.setFecha(fecha);
                cp.setIdCliente(cliente.getIdCliente());
                cp.setIdFormaPago(formaPago.getIdFormaPago());
                //Se actualiza la cabecera con los datos que proporcione el Usuario
                datos.actualizarCabeceraPedido(cp);
                //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                finish();
                Toast.makeText(this, "Cabecera Actualizada Exitosamente", Toast.LENGTH_SHORT).show();
            } else {
                CabeceraPedido cp = new CabeceraPedido(null, fecha, cliente.getIdCliente(), formaPago.getIdFormaPago());
                datos.insertarCabeceraPedido(cp);
                //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                finish();
                Toast.makeText(this, "Cabecera Añadida Exitosamente", Toast.LENGTH_SHORT).show();
            }
        } else if( view.getId() == R.id.fab_deleteCabecera){
            //Se ejecuta el método que contiene la Ventana de Diálogo para eliminar la Cabecera
            confirmDelete(id_cabecera);
        }
    }

    public void confirmDelete(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Eliminar");
        builder.setMessage("Está Seguro?");

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //Eliminar Cabecera de Pedido
                datos.eliminarCabeceraPedido(id);
                //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                finish();
                //Se muestra un mensaje que indica que el Detalle de Pedido se ha eliminado
                Toast.makeText(getApplicationContext(), "Cabecera Eliminada Exitosamente", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hacer nada
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onResume();
            }
        });
    }
}
