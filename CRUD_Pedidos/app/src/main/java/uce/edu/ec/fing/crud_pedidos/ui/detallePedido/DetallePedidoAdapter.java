package uce.edu.ec.fing.crud_pedidos.ui.detallePedido;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;


public class DetallePedidoAdapter extends ArrayAdapter<DetallePedido> {


    public DetallePedidoAdapter(Context context, List<DetallePedido> objects) {super(context,0, objects);}

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        DetallePedido detallPedido = getItem(position);

        OperacionesBaseDatos db = new OperacionesBaseDatos();

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_detallepedido, parent, false);
        }

        //Obteniendo views
        //TextView textViewID = convertView.findViewById(R.id.tv_id_producto);
        TextView textViewCabecera = convertView.findViewById(R.id.tv_idCabeceraDetalle);
        TextView textViewProducto = convertView.findViewById(R.id.tv_productoDetalle);
        TextView textViewCantidad = convertView.findViewById(R.id.tv_cantidadDetalle);
        TextView textViewPrecio = convertView.findViewById(R.id.tv_precioDetalle);


        String idCabeceraDetalle = "Factura Nº: "+String.valueOf(detallPedido.getIdCabeceraPedido());
        textViewCabecera.setText(idCabeceraDetalle);

        String idProductoDetalle = "Producto: "+ db.obtenerProductoID(detallPedido.getIdProducto()).getNombre();
        textViewProducto.setText(idProductoDetalle);

        //Precio del Producto
        String cantidadDetallePedido = "Cantidad: "+String.valueOf(detallPedido.getCantidad());
        textViewCantidad.setText(cantidadDetallePedido);
        //Existencias del Producto
        String precioDetallePedido = "Precio: "+String.valueOf(detallPedido.getPrecio());
        textViewPrecio.setText(precioDetallePedido);

        return convertView;
    }
}

