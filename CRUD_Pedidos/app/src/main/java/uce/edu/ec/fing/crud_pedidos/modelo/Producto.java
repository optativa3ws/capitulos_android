package uce.edu.ec.fing.crud_pedidos.modelo;


public class Producto {

    public String idProducto;

    public String nombre;

    public float precio;

    public int existencias;

    public int estado;

    public Producto(String idProducto, String nombre, float precio, int existencias, int estado) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
        this.existencias = existencias;
        this.estado = estado;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        //Salida para mostrar cada @Producto en la lista
        return "Producto: "+ nombre +"\nPrecio: "+precio+"\nExistencias: "+existencias;
    }
}
