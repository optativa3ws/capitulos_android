package uce.edu.ec.fing.crud_pedidos.ui.formaPago;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.FormaPago;

public class FormaPagoAdapter extends ArrayAdapter<FormaPago> {

    public FormaPagoAdapter(Context context, List<FormaPago> objects) {super(context,0, objects);}
    int textColorId;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        FormaPago formaPago = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_formapago, parent, false);
        }

        //Obteniendo views
        //TextView textViewID = convertView.findViewById(R.id.tv_id_producto);
        TextView textViewNombre = convertView.findViewById(R.id.tv_nombre_formapago);

        //Añadiendo datos a textview
        //textViewID.setText(formaPago.getIdProducto());
        //Nombre de la Forma de Pago
        textViewNombre.setText(formaPago.getNombre());

        //Cambiar el color si el estado es igual a 0 (inactivo)
        if(getItem(position).getEstado() == 0){
            textColorId = Color.RED;
        }else {
            int nightModeFlags =
                    getContext().getResources().getConfiguration().uiMode &
                            Configuration.UI_MODE_NIGHT_MASK;
            switch (nightModeFlags) {
                case Configuration.UI_MODE_NIGHT_YES:
                    textColorId = Color.WHITE;
                    break;

                case Configuration.UI_MODE_NIGHT_NO:
                    textColorId = Color.BLACK;
                    break;
            }
        }
        textViewNombre.setTextColor(textColorId);

        return convertView;
    }
}
