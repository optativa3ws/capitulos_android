package uce.edu.ec.fing.crud_pedidos.ui.cabeceraPedido;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;
import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.crud_pedidos.ui.cliente.ClienteAddEditActivity;

public class CabeceraPedidoFragment extends Fragment {

    private OperacionesBaseDatos datos;
    private ListView listViewCabeceraPedidos;
    private CabeceraPedidoViewModel cabeceraPedidoViewModel;
    private CabeceraPedidoAdapter cabeceraPedidoAdapter;
    private List<CabeceraPedido> listCabeceraPedido;


    public static CabeceraPedidoFragment newInstance() {
        Bundle args = new Bundle();
        CabeceraPedidoFragment exampleFragment = new CabeceraPedidoFragment();
        exampleFragment.setArguments(args);
        return exampleFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        datos = OperacionesBaseDatos.obtenerInstancia(getActivity());

        cabeceraPedidoViewModel =
                new ViewModelProvider(this).get(CabeceraPedidoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_cabecera_pedido, container, false);

        //Si el Boton FLotante se presiona:
        FloatingActionButton fab = root.findViewById(R.id.fab_addCabecera);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CabeceraPedidoAddEditActivity.class);
                startActivity(intent);
            }
        });
        //Se instancia el listView y si se presiona un item
        listViewCabeceraPedidos = root.findViewById(R.id.list_cabecera);
        listViewCabeceraPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                CabeceraPedido currentItemEdit = (CabeceraPedido)parent.getItemAtPosition(position);
                String currentCabeceraIDedit = currentItemEdit.getIdCabeceraPedido();
                Intent intent = new Intent(getContext(), CabeceraPedidoAddEditActivity.class);
                intent.putExtra("id_cabecera", currentCabeceraIDedit);
                startActivity(intent);
            }
        });
        cabeceraPedidoViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listarCabeceraPedido();
            }
        });
        return root;
    }


    //Método para Listar Cabeceras
    public void listarCabeceraPedido() {
        /*..................................................................*/
        listCabeceraPedido = datos.obtenerCabecerasPedidos2(); //lista de Cabeceras que se mostrarán en listview
        cabeceraPedidoAdapter = new CabeceraPedidoAdapter(getActivity(), listCabeceraPedido);
        listViewCabeceraPedidos.setAdapter(cabeceraPedidoAdapter);
        /*...................................................................*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //if the data has changed
        this.listCabeceraPedido.clear();
        listarCabeceraPedido();
        cabeceraPedidoAdapter.notifyDataSetChanged();
    }

}
