package uce.edu.ec.fing.crud_pedidos.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.ContratoPedidos.CabecerasPedido;
import uce.edu.ec.fing.crud_pedidos.sqlite.ContratoPedidos.Clientes;
import uce.edu.ec.fing.crud_pedidos.sqlite.ContratoPedidos.DetallesPedido;
import uce.edu.ec.fing.crud_pedidos.sqlite.ContratoPedidos.FormasPago;
import uce.edu.ec.fing.crud_pedidos.sqlite.ContratoPedidos.Productos;

/**
 * Clase que administra la conexión de la base de datos y su estructuración
 */
public class BaseDatosPedidos extends SQLiteOpenHelper {

    private static final String NOMBRE_BASE_DATOS = "CRUDPedidos.db";

    private static final int VERSION_ACTUAL = 1;

    private final Context contexto;

    interface Tablas {
        String CABECERA_PEDIDO = "cabecera_pedido";
        String DETALLE_PEDIDO = "detalle_pedido";
        String PRODUCTO = "producto";
        String CLIENTE = "cliente";
        String FORMA_PAGO = "forma_pago";
    }

    interface Referencias {

        String ID_CABECERA_PEDIDO = String.format("REFERENCES %s(%s) ON DELETE CASCADE",
                Tablas.CABECERA_PEDIDO, CabecerasPedido.ID);

        String ID_PRODUCTO = String.format("REFERENCES %s(%s)",
                Tablas.PRODUCTO, Productos.ID);

        String ID_CLIENTE = String.format("REFERENCES %s(%s)",
                Tablas.CLIENTE, Clientes.ID);

        String ID_FORMA_PAGO = String.format("REFERENCES %s(%s)",
                Tablas.FORMA_PAGO, FormasPago.ID);
    }

    public BaseDatosPedidos(Context contexto) {
        super(contexto, NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
        this.contexto = contexto;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                db.setForeignKeyConstraintsEnabled(true);
            } else {
                db.execSQL("PRAGMA foreign_keys=ON");
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,%s DATETIME NOT NULL,%s TEXT NOT NULL %s," +
                        "%s TEXT NOT NULL %s)",
                Tablas.CABECERA_PEDIDO, BaseColumns._ID,
                CabecerasPedido.ID, CabecerasPedido.FECHA,
                CabecerasPedido.ID_CLIENTE, Referencias.ID_CLIENTE,
                CabecerasPedido.ID_FORMA_PAGO, Referencias.ID_FORMA_PAGO));

        db.execSQL(String.format("CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT UNIQUE NOT NULL,"+
                        "%s TEXT NOT NULL %s," +
                        "%s TEXT NOT NULL %s, " +
                        "%s INTEGER NOT NULL," +
                        "%s REAL NOT NULL)",
                Tablas.DETALLE_PEDIDO, BaseColumns._ID,
                DetallesPedido.ID,
                DetallesPedido.ID_CABECERA, Referencias.ID_CABECERA_PEDIDO,
                DetallesPedido.ID_PRODUCTO, Referencias.ID_PRODUCTO,
                DetallesPedido.CANTIDAD,
                DetallesPedido.PRECIO
        ));

        db.execSQL(String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT NOT NULL UNIQUE,%s TEXT NOT NULL,%s REAL NOT NULL," +
                        "%s INTEGER NOT NULL CHECK(%s>=0) , %s INTEGER NOT NULL )",
                Tablas.PRODUCTO, BaseColumns._ID,
                Productos.ID, Productos.NOMBRE, Productos.PRECIO,
                Productos.EXISTENCIAS, Productos.EXISTENCIAS, Productos.ESTADO));

        db.execSQL(String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT NOT NULL UNIQUE,%s TEXT NOT NULL,%s TEXT NOT NULL,%s TEXT NOT NULL,%s INTEGER NOT NULL)",
                Tablas.CLIENTE, BaseColumns._ID,
                Clientes.ID, Clientes.NOMBRES, Clientes.APELLIDOS, Clientes.TELEFONO, Clientes.ESTADO));

        db.execSQL(String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "%s TEXT NOT NULL UNIQUE,%s TEXT NOT NULL,%s INTEGER NOT NULL)",
                Tablas.FORMA_PAGO, BaseColumns._ID,
                FormasPago.ID, FormasPago.NOMBRE, FormasPago.ESTADO));

        //Agregar Clientes por defecto
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Veronica', 'Del Topo', '4552000', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Carlos', 'Villagran', '4440000', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Mario', 'Paredes', '3123849', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Pedro', 'Lopez', '2933288', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Sofia', 'Martínez', '2348328', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Elias', 'Moreno', '2323429', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Gabriel', 'Orosco', '3428829', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Wendy', 'Gaona', '3193499', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Maria', 'Jiménez', '2383289', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Jaime', 'Flores', '2035930', 1)",
                Tablas.CLIENTE, Clientes.generarIdCliente()));

        //Agregar Productos por Defecto
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Mochila', 20, 100, 1)",
                Tablas.PRODUCTO, Productos.generarIdProducto()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Balón', 35.99, 100, 1)",
                Tablas.PRODUCTO, Productos.generarIdProducto()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Laptop', 499.99, 25, 1)",
                Tablas.PRODUCTO, Productos.generarIdProducto()));

        //Agregar Formas de Pago por Defecto
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Efectivo', 1)",
                Tablas.FORMA_PAGO, FormasPago.generarIdFormaPago()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Tarjeta de Crédito', 1)",
                Tablas.FORMA_PAGO, FormasPago.generarIdFormaPago()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Tarjeta de Débito', 1)",
                Tablas.FORMA_PAGO, FormasPago.generarIdFormaPago()));
        db.execSQL(String.format("INSERT INTO %s VALUES (null, '%s' , 'Cheque', 1)",
                Tablas.FORMA_PAGO, FormasPago.generarIdFormaPago()));

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + Tablas.CABECERA_PEDIDO);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.DETALLE_PEDIDO);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.PRODUCTO);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.CLIENTE);
        db.execSQL("DROP TABLE IF EXISTS " + Tablas.FORMA_PAGO);

        onCreate(db);
    }

}