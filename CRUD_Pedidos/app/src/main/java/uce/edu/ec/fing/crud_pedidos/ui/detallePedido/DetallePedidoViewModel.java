package uce.edu.ec.fing.crud_pedidos.ui.detallePedido;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DetallePedidoViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public DetallePedidoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
