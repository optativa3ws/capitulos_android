package uce.edu.ec.fing.crud_pedidos.ui.cabeceraPedido;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CabeceraPedidoViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public CabeceraPedidoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
