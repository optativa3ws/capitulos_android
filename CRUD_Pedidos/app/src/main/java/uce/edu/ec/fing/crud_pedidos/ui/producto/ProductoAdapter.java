package uce.edu.ec.fing.crud_pedidos.ui.producto;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;

import static androidx.core.content.ContextCompat.startActivity;

public class ProductoAdapter extends ArrayAdapter<Producto> {

    public ProductoAdapter(Context context, List<Producto> objects) {super(context,0, objects);}
    int textColorId;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        Producto producto = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_producto, parent, false);
        }

        //Obteniendo views
        //TextView textViewID = convertView.findViewById(R.id.tv_id_producto);
        TextView textViewNombre = convertView.findViewById(R.id.tv_nombre);
        TextView textViewPrecio = convertView.findViewById(R.id.tv_precio);
        TextView textViewExistencia = convertView.findViewById(R.id.tv_existencia);

        //Añadiendo datos a textview
        //textViewID.setText(producto.getIdProducto());
        //Nombre del Producto
        String nombreProducto = producto.getNombre();
        textViewNombre.setText(nombreProducto);
        //Precio del Producto
        String precioProducto = "Precio: "+String.valueOf(producto.getPrecio());
        textViewPrecio.setText(precioProducto);
        //Existencias del Producto
        String existenciasProducto = "Existencias: "+String.valueOf(producto.getExistencias());
        textViewExistencia.setText(existenciasProducto);

        //Si el estado del Producto es 0, se cambia el color para identificarlo
        if(getItem(position).getEstado() == 0){
            textColorId = Color.RED;
        }else {
            int nightModeFlags =
                    getContext().getResources().getConfiguration().uiMode &
                            Configuration.UI_MODE_NIGHT_MASK;
            switch (nightModeFlags) {
                case Configuration.UI_MODE_NIGHT_YES:
                    textColorId = Color.WHITE;
                    break;

                case Configuration.UI_MODE_NIGHT_NO:
                    textColorId = Color.BLACK;
                    break;
            }
        }
        textViewNombre.setTextColor(textColorId);
        textViewPrecio.setTextColor(textColorId);
        textViewExistencia.setTextColor(textColorId);

        return convertView;
    }
}
