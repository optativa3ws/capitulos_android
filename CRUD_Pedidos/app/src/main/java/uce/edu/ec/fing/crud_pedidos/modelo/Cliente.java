package uce.edu.ec.fing.crud_pedidos.modelo;

public class Cliente {
 
    public String idCliente;

    public String nombres;

    public String apellidos;

    public String telefono;

    public int estado;

    public Cliente(String idCliente, String nombres, String apellidos, String telefono, int estado) {
        this.idCliente = idCliente;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.estado=estado;
    }


    public Cliente(String nombres, String apellidos) {

        this.nombres = nombres;
        this.apellidos = apellidos;
    }

    public Cliente(String nombres, String apellidos, String telefono, int estado) {

        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.estado=estado;
    }

    public Cliente() {

    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Nombres: " + nombres + " " + apellidos + ". " + "Teléfono: " + telefono;
    }


}
