package uce.edu.ec.fing.crud_pedidos.ui.cliente;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.crud_pedidos.ui.producto.ProductoAddEditActivity;

public class ClienteFragment extends Fragment {

    private OperacionesBaseDatos datos;
    private ListView listViewCliente;
    private ClienteViewModel clienteViewModel;
    private ClienteAdapter clienteAdapter;
    private List<Cliente> listClientes;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        datos = OperacionesBaseDatos.obtenerInstancia(getActivity());

        clienteViewModel =
                new ViewModelProvider(this).get(ClienteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_clientes, container, false);
        //Si el Boton FLotante se presiona:
        FloatingActionButton fab = root.findViewById(R.id.fab_addCliente);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ClienteAddEditActivity.class);
                startActivity(intent);
            }
        });
        //Se instancia el listView y si se presiona un item
        listViewCliente = root.findViewById(R.id.list_clientes);
        listViewCliente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                Cliente currentItemEdit = (Cliente)parent.getItemAtPosition(position);
                String currentClienteIDedit = currentItemEdit.getIdCliente();
                Intent intent = new Intent(getContext(), ClienteAddEditActivity.class);
                intent.putExtra("id_cliente", currentClienteIDedit);
                startActivity(intent);
            }
        });
        clienteViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listarClientes();
            }
        });
        return root;
    }

    //Método para Listar Clientes
    public void listarClientes() {
        /*..................................................................*/
        listClientes = datos.obtenerClientes2(); //lista de Clientes que se mostrarán en listview
        clienteAdapter= new ClienteAdapter(getActivity(), listClientes);
        listViewCliente.setAdapter(clienteAdapter);
        /*...................................................................*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //if the data has changed
        this.listClientes.clear();
        listarClientes();
        clienteAdapter.notifyDataSetChanged();
    }

}