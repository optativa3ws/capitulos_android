package uce.edu.ec.fing.crud_pedidos.ui.cliente;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.switchmaterial.SwitchMaterial;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class ClienteAddEditActivity extends AppCompatActivity {

    private String id_cliente = null;

    private OperacionesBaseDatos datos;

    private EditText editTextNombres, editTextApellidos, editTextTelefono;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_cliente);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Se obtiene el ID del Cliente seleccionado en caso de Actualizar
        id_cliente = getIntent().getStringExtra("id_cliente");

        //Se cambia el titulo de la vista en caso de Crear o Actualizar
        setTitle(id_cliente == null ? "Añadir CLiente" : "Editar Cliente");

        //Se crea una nueva instancia de la clase que continen los metodos para persistir los datos en la DB
        datos = OperacionesBaseDatos.obtenerInstancia(getApplicationContext());

        //Se obtiene los elementos de la Vista que vamos a utilizar
        editTextNombres = findViewById(R.id.in_nombres);
        editTextApellidos = findViewById(R.id.in_apellidos);
        editTextTelefono = findViewById(R.id.in_telefono);
        Button buttonAddEdit = findViewById(R.id.bt_addeditCliente);

        SwitchMaterial switchCliente = findViewById(R.id.switchCliente);

        //Si se va a editar un Producto seteamos los EditText con sus valores
        if (id_cliente != null) {
            Cliente cliente = datos.obtenerClientespoID2(id_cliente);
            editTextNombres.setText(cliente.getNombres());
            editTextApellidos.setText(cliente.getApellidos());
            editTextTelefono.setText(cliente.getTelefono());

            //Se muestra el switch para cambiar el estado del Cliente
            switchCliente.setVisibility(View.VISIBLE);
            switchCliente.setChecked(cliente.getEstado() == 1);

            //Se cambia el label del Botón para Actualizar el CLiente
            buttonAddEdit.setText("ACTUALIZAR");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onClick(View view) {
        if (view.getId() == R.id.bt_addeditCliente) {
            addEditCliente(view);
        }
    }

    private void addEditCliente(View view) {
        if (view.getId() == R.id.bt_addeditCliente) {
            String nombreText = editTextNombres.getText().toString();
            String apellidoText = editTextApellidos.getText().toString();
            String telefonoText = editTextTelefono.getText().toString();
            SwitchMaterial switchCliente = findViewById(R.id.switchCliente);

            //El estado por defecto es igual a 1, osea activo
            int estado = 1;
            //Si todos los campos contienen valores:
            if (!nombreText.isEmpty() && !apellidoText.isEmpty()
                    && !telefonoText.isEmpty()) {
                //Si el id del Cliente no es nulo, se va a Actualizar un Cliente
                if (id_cliente != null) {
                    Cliente client = datos.obtenerClientespoID2(id_cliente);
                    client.setNombres(nombreText);
                    client.setApellidos(apellidoText);
                    client.setTelefono(telefonoText);

                    //Si el switch no está activo, el estado se setea a 0
                    if(!switchCliente.isChecked()){
                        estado = 0;
                    }
                    client.setEstado(estado);

                    //Se actualiza el CLiente con los datos ingresados por el Usuario
                    datos.actualizarCliente(client);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    //Se lanza un mensaje al usuario con el detalle de la operación
                    Toast.makeText(this, "Cliente Actualizado Exitosamente", Toast.LENGTH_SHORT).show();
                } else {
                    //Caso contrario se CREA un Cliente con los valores ingresados por el usuario
                    Cliente client = new Cliente(null, nombreText, apellidoText, telefonoText, estado);
                    datos.insertarCliente(client);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    Toast.makeText(this, "Cliente Añadido Exitosamente", Toast.LENGTH_SHORT).show();
                }
            } else {
                //Caso contrario se manda un mensaje al usuario que ingrese valores.
                Toast.makeText(getApplicationContext(), "Ingrese todos los campos, por favor", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
