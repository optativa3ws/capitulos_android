package uce.edu.ec.fing.crud_pedidos.ui.producto;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class ProductAddEditActivity extends AppCompatActivity {

    private String id_cabecera = null;

    public static final int REQUEST_ADD_CABECERA = 1;

    private String fecha;
    EditText nombreText, precioText, exisText;

    private OperacionesBaseDatos datos;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_producto);

        datos = OperacionesBaseDatos.obtenerInstancia(getApplicationContext());


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onClick(View view){
        if(view.getId() == R.id.bt_addeditProducto){
            addEditProducto(view);
        }
    }

    private void addEditProducto(View view) {
        switch (view.getId()) {
            case R.id.bt_addeditProducto:
                nombreText = findViewById(R.id.in_nombre);
                precioText = findViewById(R.id.in_precio);
                exisText = findViewById(R.id.in_existencias);
                if (id_cabecera != null) {
                    Producto prod = datos.obtenerProductoID(id_cabecera);
                    prod.setNombre(nombreText.getText().toString());
                    prod.setPrecio(Float.parseFloat(precioText.getText().toString()));
                    prod.setExistencias(Integer.parseInt(exisText.getText().toString()));
                    datos.actualizarProducto(prod);
                    Toast.makeText(this, "Producto Actualizado Exitosamente", Toast.LENGTH_SHORT).show();
                } else {
                    int estado = 1;
                    Producto prod = new Producto(null, nombreText.getText().toString(), Float.parseFloat(precioText.getText().toString()), Integer.parseInt(exisText.getText().toString()), estado);
                    datos.insertarProducto(prod);
                    Toast.makeText(this, "Cabecera Añadida Exitosamente", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
