package uce.edu.ec.fing.crud_pedidos.modelo;

public class FormaPago {

    public String idFormaPago;

    public String nombre;
    public int estado;

    //constructor que se llama cuando se va a actualizar y se requiere el id
    public FormaPago(String idFormaPago, String nombre, int estado) {
        this.idFormaPago = idFormaPago;
        this.nombre = nombre;
        this.estado = estado;
    }


    public FormaPago(String nombre, int estado) {

        this.nombre = nombre;
        this.estado = estado;
    }

    public String getIdFormaPago() {
        return idFormaPago;
    }

    public void setIdFormaPago(String idFormaPago) {
        this.idFormaPago = idFormaPago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
