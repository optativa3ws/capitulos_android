package uce.edu.ec.fing.crud_pedidos.ui.producto;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.switchmaterial.SwitchMaterial;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class ProductoAddEditActivity extends AppCompatActivity {

    private String id_producto = null;

    private OperacionesBaseDatos datos;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_producto);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Se obtiene el ID del Producto seleccionado en caso de Actualizar
        id_producto = getIntent().getStringExtra("id_producto");

        //Se cambia el titulo de la vista en caso de Crear o Actualizar
        setTitle(id_producto == null ? "Añadir Producto" : "Editar Producto");

        //Se crea una nueva instancia de la clase que continen los metodos para persistir los datos en la DB
        datos = OperacionesBaseDatos.obtenerInstancia(getApplicationContext());

        //Se obtiene los elementos de la Vista que vamos a utilizar
        EditText editTextNombre = findViewById(R.id.in_nombre);
        EditText editTextPrecio = findViewById(R.id.in_precio);
        EditText editTextExistencias = findViewById(R.id.in_existencias);

        Button buttonAddEdit = findViewById(R.id.bt_addeditProducto);
        SwitchMaterial switchEstado = findViewById(R.id.switchProducto);

        //Si se va a editar un Producto seteamos los EditText con sus valores
        if (id_producto != null) {
            Producto producto = datos.obtenerProductoID(id_producto);
            //Se setea los textEdit con los valores del Producto seleccionado
            editTextNombre.setText(producto.getNombre());
            editTextPrecio.setText(String.valueOf(producto.getPrecio()));
            editTextExistencias.setText(String.valueOf(producto.getExistencias()));

            //Se muestra el switch para cambiar el estado del Producto
            switchEstado.setVisibility(View.VISIBLE);
            switchEstado.setChecked(producto.getEstado() == 1);


            //Actualizar label del botón para que actualice el Producto
            buttonAddEdit.setText("ACTUALIZAR");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onClick(View view) {
        if (view.getId() == R.id.bt_addeditProducto) {
            addEditProducto(view);
        }
    }

    private void addEditProducto(View view) {
        if (view.getId() == R.id.bt_addeditProducto) {
            EditText nombreText = findViewById(R.id.in_nombre);
            EditText precioText = findViewById(R.id.in_precio);
            EditText exisText = findViewById(R.id.in_existencias);
            SwitchMaterial switchEstado = findViewById(R.id.switchProducto);

            //El estado por defecto es igual a 1, es decir el Proucto va a estar activo
            int estado = 1;

            //Si todos los campos están llenos, se continua
            if (!nombreText.getText().toString().isEmpty() && !precioText.getText().toString().isEmpty()
                    && !exisText.getText().toString().isEmpty()) {
                if (id_producto != null) {
                    //Se obtiene el producto que el usuario desea Actualizar
                    Producto prod = datos.obtenerProductoID(id_producto);
                    //Se setea los valores del producto con los nuevos valores que ingresa el usuario
                    prod.setNombre(nombreText.getText().toString());
                    prod.setPrecio(Float.parseFloat(precioText.getText().toString()));
                    prod.setExistencias(Integer.parseInt(exisText.getText().toString()));

                    //Si el switch no está activo, el estado se setea a 0
                    if(!switchEstado.isChecked()){
                        estado = 0;
                    }
                    prod.setEstado(estado);

                    //Se actualiza el Producto con los valores ya seteados
                    if(datos.actualizarProducto(prod)){
                        //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                        finish();
                        //Se lanza un mensaje al usuario con el detalle de la operación
                        Toast.makeText(this, "Producto Actualizado Exitosamente", Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Ocurrió un error, Contacte al desarrollador", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Se crea un nuevo Producto con los valores ingresados por el Usuario
                    Producto prod = new Producto(null, nombreText.getText().toString(), Float.parseFloat(precioText.getText().toString()), Integer.parseInt(exisText.getText().toString()), estado);
                    datos.insertarProducto(prod);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    Toast.makeText(this, "Producto Añadido Exitosamente", Toast.LENGTH_SHORT).show();
                }
            } else {
                //Caso contrario se manda un mensaje para que ingrese todos los campos
                Toast.makeText(getApplicationContext(), "Ingrese todos los campos, por favor", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
