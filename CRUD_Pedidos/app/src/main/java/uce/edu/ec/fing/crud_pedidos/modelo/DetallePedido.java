package uce.edu.ec.fing.crud_pedidos.modelo;

public class DetallePedido {

    public String idDetallePedido; //secuencia

    public String idCabeceraPedido;

    public String idProducto;

    public int cantidad;

    public float precio;

    public CabeceraPedido cabeceraPedidoRef;

    public Producto productoRef;

    public DetallePedido(String idDetallePedido, String idCabeceraPedido,
                         String idProducto, int cantidad, float precio) {
        this.idDetallePedido = idDetallePedido;
        this.idCabeceraPedido = idCabeceraPedido;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public DetallePedido(String idCabeceraPedido,
                         String idProducto, int cantidad, float precio) {

        this.idCabeceraPedido = idCabeceraPedido;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public DetallePedido() {
    }

    public DetallePedido(String idDetallePedido, CabeceraPedido cabeceraPedidoRef, Producto producto, int cantidad, float precio) {
        this.idDetallePedido = idDetallePedido;
        this.productoRef = producto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.cabeceraPedidoRef = cabeceraPedidoRef;
        this.idCabeceraPedido = cabeceraPedidoRef.idCabeceraPedido;
        this.idProducto = producto.idProducto;
    }

    public String getIdDetallePedido() {
        return idDetallePedido;
    }

    public void setIdDetallePedido(String idDetallePedido) {
        this.idDetallePedido = idDetallePedido;
    }

    public String getIdCabeceraPedido() {
        return idCabeceraPedido;
    }

    public void setIdCabeceraPedido(String idCabeceraPedido) {
        this.idCabeceraPedido = idCabeceraPedido;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public CabeceraPedido getCabeceraPedidoRef() {
        return cabeceraPedidoRef;
    }

    public void setCabeceraPedidoRef(CabeceraPedido cabeceraPedidoRef) {
        this.cabeceraPedidoRef = cabeceraPedidoRef;
    }

    public Producto getProductoRef() {
        return productoRef;
    }

    public void setProductoRef(Producto productoRef) {
        this.productoRef = productoRef;
    }

    @Override
    public String toString() {
        return "La Cabecera: " + cabeceraPedidoRef + " compró: " + cantidad + " " + productoRef + " con subtotal de: " + precio;
    }
}
