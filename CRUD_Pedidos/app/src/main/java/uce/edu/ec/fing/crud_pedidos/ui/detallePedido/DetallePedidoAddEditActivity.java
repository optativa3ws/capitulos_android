package uce.edu.ec.fing.crud_pedidos.ui.detallePedido;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.MainActivity;
import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;
import uce.edu.ec.fing.crud_pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.crud_pedidos.modelo.FormaPago;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class DetallePedidoAddEditActivity extends AppCompatActivity {

    private OperacionesBaseDatos datos;

    private String id_detalle = null;

    private Spinner spinnerCabecera, spinnerProducto;

    private EditText txtCantidad, txtPrecio;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_detallepedido);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        id_detalle = getIntent().getStringExtra("id_detalle");

        setTitle(id_detalle == null ? "Añadir Cabecera Pedido" : "Editar Cabecera Pedido");

        datos = OperacionesBaseDatos.obtenerInstancia(getApplicationContext());

        Button buttonAddEditDetalle = findViewById(R.id.bt_addeditDetallePedido);

        spinnerCabecera = findViewById(R.id.spinner_idCabeceraDetalle);
        spinnerProducto = findViewById(R.id.spinner_productoDetalle);

        txtCantidad = findViewById(R.id.in_cantidadDetalle);
        txtPrecio = findViewById(R.id.in_precioDetalle);

        //Poner el precio final automaticamente cuando se tipea la cantidad
        txtCantidad.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    float precio = datos.getProductoporNombre(spinnerProducto.getSelectedItem().toString()).getPrecio();
                    txtPrecio.setText(String.valueOf(precio * Integer.parseInt(txtCantidad.getText().toString())));
                }catch (NumberFormatException ex){}

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        loadCabeceraSpinnerData();
        loadProductoSpinnerData();

        if(id_detalle != null){
            DetallePedido detalle = datos.getDetallePedido(id_detalle);
            txtPrecio.setText(String.valueOf(detalle.getPrecio()));
            txtCantidad.setText(String.valueOf(detalle.getCantidad()));

            FloatingActionButton fab_deleteDetalle = findViewById(R.id.fab_deleteDetalle);
            fab_deleteDetalle.setVisibility(View.VISIBLE);

            buttonAddEditDetalle.setText("ACTUALIZAR");
        }

    }


    private void loadCabeceraSpinnerData() {
        List<String> labels = datos.getAllCabeceras();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerCabecera.setAdapter(dataAdapter);
    }

    private void loadProductoSpinnerData() {
        List<String> labels = datos.getAllProductosActivos();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinnerProducto.setAdapter(dataAdapter);

    }

    public void onClick(View view) {
        CabeceraPedido cabeceraPedido = datos.getCabeceraPedido(spinnerCabecera.getSelectedItem().toString());
        Producto producto = datos.getProductoporNombre(spinnerProducto.getSelectedItem().toString());
        if (view.getId() == R.id.bt_addeditDetallePedido) {
            //Si todos los campos contienen valores:
            if (!txtCantidad.getText().toString().isEmpty() && !txtPrecio.getText().toString().isEmpty()) {
                if (id_detalle != null) {
                    DetallePedido dp = datos.getDetallePedido(id_detalle);
                    System.out.println(dp);
                    dp.setIdCabeceraPedido(cabeceraPedido.getIdCabeceraPedido());
                    dp.setIdProducto(producto.getIdProducto());
                    dp.setCantidad(Integer.parseInt(txtCantidad.getText().toString()));
                    dp.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                    datos.actualizarDetallePedido(dp);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    Toast.makeText(this, "Detalle Actualizado Exitosamente", Toast.LENGTH_SHORT).show();
                } else {
                    DetallePedido dp = new DetallePedido(null,
                            cabeceraPedido.getIdCabeceraPedido(), producto.getIdProducto(),
                            Integer.parseInt(txtCantidad.getText().toString()),
                            Float.parseFloat(txtPrecio.getText().toString()));
                    datos.insertarDetallePedido(dp);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    Toast.makeText(this, "Detalle Añadido Exitosamente", Toast.LENGTH_SHORT).show();
                }
            }else{
                //Caso contrario se manda un mensaje al usuario que ingrese valores.
                Toast.makeText(getApplicationContext(), "Ingrese todos los campos, por favor",
                        Toast.LENGTH_SHORT).show();
            }
        } else if(view.getId() == R.id.fab_deleteDetalle){
            //Se ejecuta el método que contiene la Ventana de Diálogo para eliminar el Detalle
            confirmDelete(id_detalle);
        }
    }

    public void confirmDelete(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Eliminar");
        builder.setMessage("Está Seguro?");

        builder.setPositiveButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Eliminar Detalle de Pedido
                datos.eliminarDetallePedido(id);
                //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                finish();
                //Se muestra un mensaje que indica que el Detalle de Pedido se ha eliminado
                Toast.makeText(getApplicationContext(), "Detalle de Pedido Eliminado Exitosamente", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hacer nada
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onResume();
            }
        });
    }
}
