package uce.edu.ec.fing.crud_pedidos.ui.producto;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class ProductoFragment extends Fragment{

    private ProductoViewModel productoViewModel;
    private OperacionesBaseDatos datos;
    private ListView listViewProducto;
    private ProductoAdapter arrayAdapterProducto; //Adaptador para enviar a una list y presentarlo en un listview
    private List<Producto> listProductos;

    ImageButton buttonEdit;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        datos = OperacionesBaseDatos.obtenerInstancia(getActivity());

        productoViewModel =
                new ViewModelProvider(this).get(ProductoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_productos, container, false);
        //Si el Boton FLotante se presiona:
        FloatingActionButton fab = root.findViewById(R.id.fab_addProduct);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ProductoAddEditActivity.class);
                startActivity(intent);
            }
        });
        //Se instancia el listView y si se presiona un item
        listViewProducto = root.findViewById(R.id.list_productos);
        listViewProducto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                Producto currentItemEdit = (Producto)parent.getItemAtPosition(position);
                String currentProductoIDedit = currentItemEdit.getIdProducto();
                Intent intent = new Intent(getContext(), ProductoAddEditActivity.class);
                intent.putExtra("id_producto", currentProductoIDedit);
                startActivity(intent);
            }
        });
        productoViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listarProductos();
            }
        });

        return root;
    }

    //Método para Listar Productos
    public void listarProductos() {
        /*..................................................................*/
        listProductos = datos.obtenerProductos(); //lista de Productos que se mostrarán en listview
        arrayAdapterProducto = new ProductoAdapter(getActivity(), listProductos);
        listViewProducto.setAdapter(arrayAdapterProducto);
        /*...................................................................*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //if the data has changed
        this.listProductos.clear();
        listarProductos();
        arrayAdapterProducto.notifyDataSetChanged();
    }
}