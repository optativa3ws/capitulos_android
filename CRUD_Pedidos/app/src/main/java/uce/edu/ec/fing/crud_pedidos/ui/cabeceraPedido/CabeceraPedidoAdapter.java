package uce.edu.ec.fing.crud_pedidos.ui.cabeceraPedido;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class CabeceraPedidoAdapter extends ArrayAdapter<CabeceraPedido> {

    public CabeceraPedidoAdapter(Context context, List<CabeceraPedido> objects) {super(context,0, objects);}
    int textColorId;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        CabeceraPedido cabeceraPedido = getItem(position);

        OperacionesBaseDatos db = new OperacionesBaseDatos();

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_cabecera, parent, false);
        }

        //Obteniendo vistas
        //TextView textViewID = convertView.findViewById(R.id.tv_id_cliente);
        TextView textViewIdCabecera = convertView.findViewById(R.id.tv_idcabecera);
        TextView textViewIdCliente = convertView.findViewById(R.id.tv_idcliente);
        TextView textViewIdFormaPago = convertView.findViewById(R.id.tv_idformapago);
        TextView textViewFecha = convertView.findViewById(R.id.tv_fecha);

        //Añadiendo datos a vistas
        //textViewID.setText(cliente.getIdCliente());
        //ID Cabecera
        String idCabecera = cabeceraPedido.getIdCabeceraPedido();
        textViewIdCabecera.setText(idCabecera);
        //ID Cliente
        String infCliente = "Cliente: " + db.obtenerClientespoID2(cabeceraPedido.getIdCliente()).getNombres()
                + " "+ db.obtenerClientespoID2(cabeceraPedido.getIdCliente()).getApellidos();
        textViewIdCliente.setText(infCliente);
        //ID Forma de pago
        String pago = "Pago: "+ db.obtenerFormaID(cabeceraPedido.getIdFormaPago()).getNombre();
        textViewIdFormaPago.setText(pago);
        //Fecha
        String fecha = "Fecha: "+cabeceraPedido.getFecha();
        textViewFecha.setText(fecha);

        /*//Cambiar el color si el estado es igual a 0 (inactivo)
        if(getItem(position).getEstado() == 0){
            textColorId = Color.RED;
        }else {
            int nightModeFlags =
                    getContext().getResources().getConfiguration().uiMode &
                            Configuration.UI_MODE_NIGHT_MASK;
            switch (nightModeFlags) {
                case Configuration.UI_MODE_NIGHT_YES:
                    textColorId = Color.WHITE;
                    break;

                case Configuration.UI_MODE_NIGHT_NO:
                    textColorId = Color.BLACK;
                    break;
            }
        }
        textViewIdCabecera.setTextColor(textColorId);
        textViewIdCliente.setTextColor(textColorId);
        textViewIdFormaPago.setTextColor(textColorId);
        textViewFecha.setTextColor(textColorId);*/

        return convertView;
    }

}
