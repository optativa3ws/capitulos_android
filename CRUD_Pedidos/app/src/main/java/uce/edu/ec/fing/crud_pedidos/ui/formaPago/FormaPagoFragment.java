package uce.edu.ec.fing.crud_pedidos.ui.formaPago;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.FormaPago;
import uce.edu.ec.fing.crud_pedidos.modelo.Producto;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.crud_pedidos.ui.producto.ProductoAddEditActivity;

public class FormaPagoFragment extends Fragment {

    private FormaPagoViewModel slideshowViewModel;

    private OperacionesBaseDatos datos;
    private ListView listViewFormaPago;
    private FormaPagoAdapter formaPagoAdapter;
    private List<FormaPago> listFormaPago;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        datos = OperacionesBaseDatos.obtenerInstancia(getActivity());

        slideshowViewModel =
                new ViewModelProvider(this).get(FormaPagoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_forma_pago, container, false);
        //Si el Boton FLotante se presiona:
        FloatingActionButton fab = root.findViewById(R.id.fab_addFormaPago);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FormaPagoAddEditActivity.class);
                startActivity(intent);
            }
        });
        //Se instancia el listView y si se presiona un item
        listViewFormaPago = root.findViewById(R.id.list_forma_pago);
        listViewFormaPago.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                FormaPago currentItemEdit = (FormaPago)parent.getItemAtPosition(position);
                String currentFormaPagoIDedit = currentItemEdit.getIdFormaPago();
                Intent intent = new Intent(getContext(), FormaPagoAddEditActivity.class);
                intent.putExtra("id_formaPago", currentFormaPagoIDedit);
                startActivity(intent);
            }
        });
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listarFormaPago();
            }
        });
        return root;
    }

    //Método para Listar Forma de Pago
    public void listarFormaPago() {
        /*..................................................................*/
        listFormaPago = datos.obtenerFormaPago2(); //lista de Formas de Pago que se mostrarán en listview
        formaPagoAdapter= new FormaPagoAdapter(getActivity(), listFormaPago);
        listViewFormaPago.setAdapter(formaPagoAdapter);
        /*...................................................................*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //if the data has changed
        this.listFormaPago.clear();
        listarFormaPago();
        formaPagoAdapter.notifyDataSetChanged();
    }
}