package uce.edu.ec.fing.crud_pedidos.ui.cliente;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.Cliente;

public class ClienteAdapter extends ArrayAdapter<Cliente> {

    public ClienteAdapter(Context context, List<Cliente> objects) {super(context,0, objects);}

    int textColorId;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //getting employee of the specified position
        Cliente cliente = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_layout_cliente, parent, false);
        }

        //Obteniendo vistas
        //TextView textViewID = convertView.findViewById(R.id.tv_id_cliente);
        TextView textViewNombre = convertView.findViewById(R.id.tv_nombresapellidos);
        TextView textViewTelefono = convertView.findViewById(R.id.tv_telefono);

        //Añadiendo datos a vistas
        //textViewID.setText(cliente.getIdCliente());
        String nombresApellidos = cliente.getNombres() + " " + cliente.getApellidos();
        textViewNombre.setText(nombresApellidos);
        //Teléfono del Cliente
        String telefonoCliente = "Teléfono: "+ cliente.getTelefono();
        textViewTelefono.setText(telefonoCliente);

        //Cambiar el color si el estado es igual a 0 (inactivo)
        if(getItem(position).getEstado() == 0){
            textColorId = Color.RED;
        }else {
            int nightModeFlags =
                    getContext().getResources().getConfiguration().uiMode &
                            Configuration.UI_MODE_NIGHT_MASK;
            switch (nightModeFlags) {
                case Configuration.UI_MODE_NIGHT_YES:
                    textColorId = Color.WHITE;
                    break;

                case Configuration.UI_MODE_NIGHT_NO:
                    textColorId = Color.BLACK;
                    break;
            }
        }
        textViewNombre.setTextColor(textColorId);
        textViewTelefono.setTextColor(textColorId);

        return convertView;
    }
}
