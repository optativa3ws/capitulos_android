package uce.edu.ec.fing.crud_pedidos.ui.formaPago;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.switchmaterial.SwitchMaterial;

import uce.edu.ec.fing.crud_pedidos.R;
import uce.edu.ec.fing.crud_pedidos.modelo.FormaPago;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;

public class FormaPagoAddEditActivity extends AppCompatActivity {

    private String id_formaPago = null;

    private OperacionesBaseDatos datos;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_addedit_formapago);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Se obtiene el ID de la Forma de Pago seleccionada en caso de Actualizar
        id_formaPago = getIntent().getStringExtra("id_formaPago");

        //Se cambia el titulo de la vista en caso de Crear o Actualizar
        setTitle(id_formaPago == null ? "Añadir Forma de Pago" : "Editar Forma de Pago");

        //Se crea una nueva instancia de la clase que continen los metodos para persistir los datos en la DB
        datos = OperacionesBaseDatos.obtenerInstancia(this.getApplicationContext());

        //Se obtiene los elementos de la Vista que vamos a utilizar
        EditText editTextNombre = findViewById(R.id.in_forma_pago);
        Button buttonAddEdit = findViewById(R.id.bt_addeditFormaPago);
        SwitchMaterial switchFormaPago = findViewById(R.id.switchFormaPago);

        //Si se va a editar un Producto seteamos los EditText con sus valores
        if (id_formaPago != null) {
            FormaPago formaPago = datos.obtenerFormaID(id_formaPago);
            editTextNombre.setText(formaPago.getNombre());

            //Se muestra el switch para cambiar el estado de la Forma de Pago
            switchFormaPago.setVisibility(View.VISIBLE);
            switchFormaPago.setChecked(formaPago.getEstado() == 1);

            //Se cambia el label del Botón para Actualizar la Forma de Pago
            buttonAddEdit.setText("ACTUALIZAR");
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onClickformaPago(View view) {
        if (view.getId() == R.id.bt_addeditFormaPago) {
            addEditFormaPago(view);
        }
    }

    private void addEditFormaPago(View view) {
        if (view.getId() == R.id.bt_addeditFormaPago) {
            EditText nombreText =  findViewById(R.id.in_forma_pago);
            SwitchMaterial switchFormaPago = findViewById(R.id.switchFormaPago);

            //El estado por defecto es igual a 1, osea activo
            int estado = 1;
            //Si todos los campos contienen valores:
            if (!nombreText.getText().toString().isEmpty()) {
                //Si el id de la forma de Pago no es nulo, se va a Actualizar una Forma de Pago
                if (id_formaPago != null) {
                    //Se obtiene la Forma de Pago que el usuario desea Actualizar
                    FormaPago formaPago = datos.obtenerFormaID(id_formaPago);
                    formaPago.setNombre(nombreText.getText().toString());

                    //Si el switch no está activo, el estado se setea a 0
                    if(!switchFormaPago.isChecked()){
                        estado = 0;
                    }
                    formaPago.setEstado(estado);

                    //Se actualiza la Forma de Pago con los datos ingresados por el Usuario
                    if(datos.actualizarFormaPago(formaPago)){
                        //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                        finish();
                        //Se lanza un mensaje al usuario con el detalle de la operación
                        Toast.makeText(this, "Forma de Pago Actualizada Exitosamente",
                                Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(this, "Ocurrió un error, Contacte al desarrollador",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //Caso contrario se CREA una Forma de Pago con los valores ingresados por el usuario
                    FormaPago formaPago = new FormaPago(null, nombreText.getText().toString(), estado);
                    datos.insertarFormaPago(formaPago);
                    //Metodo para finalizar la Actividad y regresar al fragment que contiene el listado
                    finish();
                    Toast.makeText(this, "Forma de Pago Añadida Exitosamente",
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                //Caso contrario se manda un mensaje al usuario que ingrese valores.
                Toast.makeText(getApplicationContext(), "Ingrese todos los campos, por favor",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
