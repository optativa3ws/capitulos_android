package uce.edu.ec.fing.crud_pedidos.ui.formaPago;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FormaPagoViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public FormaPagoViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}