package uce.edu.ec.fing.crud_pedidos.ui.detallePedido;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import uce.edu.ec.fing.crud_pedidos.R;

import uce.edu.ec.fing.crud_pedidos.modelo.CabeceraPedido;
import uce.edu.ec.fing.crud_pedidos.modelo.DetallePedido;
import uce.edu.ec.fing.crud_pedidos.sqlite.OperacionesBaseDatos;
import uce.edu.ec.fing.crud_pedidos.ui.cabeceraPedido.CabeceraPedidoAddEditActivity;
import uce.edu.ec.fing.crud_pedidos.ui.cliente.ClienteFragment;


public class DetallePedidoFragment extends Fragment {

    private DetallePedidoViewModel detalleViewModel;
    private OperacionesBaseDatos datos;
    private ListView listViewDetallePedido;
    private DetallePedidoAdapter arrayAdapterDetallePedido; //Adaptador para enviar a una list y presentarlo en un listview

    public static DetallePedidoFragment newInstance() {
        Bundle args = new Bundle();
        DetallePedidoFragment exampleFragment = new DetallePedidoFragment();
        exampleFragment.setArguments(args);
        return exampleFragment;
    }

    List<DetallePedido> listDetallePedido;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        datos = OperacionesBaseDatos.obtenerInstancia(getActivity());

        detalleViewModel =
                new ViewModelProvider(this).get(DetallePedidoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_detallepedido, container, false);

        //Si el Boton FLotante se presiona:
        FloatingActionButton fab = root.findViewById(R.id.fab_addDetalle);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DetallePedidoAddEditActivity.class);
                startActivity(intent);
            }
        });
        //Se instancia el listView y si se presiona un item
        listViewDetallePedido = root.findViewById(R.id.list_detallepedido);
        listViewDetallePedido.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                //Toast.makeText(getContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
                DetallePedido currentItemEdit = (DetallePedido)parent.getItemAtPosition(position);
                String currentCabeceraIDedit = currentItemEdit.getIdDetallePedido();
                Intent intent = new Intent(getContext(), DetallePedidoAddEditActivity.class);
                intent.putExtra("id_detalle", currentCabeceraIDedit);
                startActivity(intent);
            }
        });

        detalleViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                listartDetallePedido();
            }
        });

        return root;
    }

    //Método para Listar Productos
    public void listartDetallePedido() {
        /*..................................................................*/
        listDetallePedido = datos.obtenerDetallesPedido(); //lista de DetallesPedidos que se mostrarán en listview
        arrayAdapterDetallePedido = new DetallePedidoAdapter(getActivity(), listDetallePedido);
        listViewDetallePedido.setAdapter(arrayAdapterDetallePedido);
        /*...................................................................*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //if the data has changed
        this.listDetallePedido.clear();
        listartDetallePedido();
        arrayAdapterDetallePedido.notifyDataSetChanged();
    }
    
}
