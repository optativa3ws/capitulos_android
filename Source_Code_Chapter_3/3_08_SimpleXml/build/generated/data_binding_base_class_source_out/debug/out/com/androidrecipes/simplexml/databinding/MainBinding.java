// Generated by data binding compiler. Do not edit!
package com.androidrecipes.simplexml.databinding;

import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.androidrecipes.simplexml.Purchase;
import com.androidrecipes.simplexml.R;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;

public abstract class MainBinding extends ViewDataBinding {
  @NonNull
  public final TextView address;

  @NonNull
  public final TextView amount;

  @NonNull
  public final LinearLayout constraintLayout;

  @NonNull
  public final TextView productName;

  @NonNull
  public final TextView timestamp;

  @NonNull
  public final TextView units;

  @Bindable
  protected Purchase mPurchase;

  @Bindable
  protected String mRawXml;

  protected MainBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView address, TextView amount, LinearLayout constraintLayout, TextView productName,
      TextView timestamp, TextView units) {
    super(_bindingComponent, _root, _localFieldCount);
    this.address = address;
    this.amount = amount;
    this.constraintLayout = constraintLayout;
    this.productName = productName;
    this.timestamp = timestamp;
    this.units = units;
  }

  public abstract void setPurchase(@Nullable Purchase purchase);

  @Nullable
  public Purchase getPurchase() {
    return mPurchase;
  }

  public abstract void setRawXml(@Nullable String rawXml);

  @Nullable
  public String getRawXml() {
    return mRawXml;
  }

  @NonNull
  public static MainBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.main, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static MainBinding inflate(@NonNull LayoutInflater inflater, @Nullable ViewGroup root,
      boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<MainBinding>inflateInternal(inflater, R.layout.main, root, attachToRoot, component);
  }

  @NonNull
  public static MainBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.main, null, false, component)
   */
  @NonNull
  @Deprecated
  public static MainBinding inflate(@NonNull LayoutInflater inflater, @Nullable Object component) {
    return ViewDataBinding.<MainBinding>inflateInternal(inflater, R.layout.main, null, false, component);
  }

  public static MainBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static MainBinding bind(@NonNull View view, @Nullable Object component) {
    return (MainBinding)bind(component, view, R.layout.main);
  }
}
